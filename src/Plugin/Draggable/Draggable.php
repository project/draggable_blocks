<?php

/**
 * @file
 * Contains \Drupal\draggable_blocks\Plugin\Draggable\DraggableDefault.
 */

namespace Drupal\draggable_blocks\Plugin\Draggable;


/**
 * Provides a default class for Draggables.
 */
class DraggableDefault extends DraggableBase {

}
