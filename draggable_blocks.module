<?php
/**
 * @file
 * Primarily Drupal hooks.
 */

use Drupal\block;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use \Drupal\draggable_blocks\Draggable;

/**
 *  Implements hook_theme().
 */
function draggable_blocks_theme() {
  return array(
    'draggable_blocks_admin' => array(
      'variables' => array('form' => NULL, 'blocks' => NULL),
    ),
    'block_list' => array(
      'variables' => array('items' => array()),
    ),
  );
}

function _draggable_blocks_control() {
  $items = _draggable_blocks_block_definitions();
  $render_list = array(
    '#theme' => 'block_list',
    '#items' => $items,
  );
  $form = \Drupal::formBuilder()->getForm('Drupal\draggable_blocks\Form\AdminForm');
  $render_array['draggable_blocks_admin'] = array(
    '#theme' => 'draggable_blocks_admin',
    '#blocks' => \Drupal::service('renderer')->render($render_list),
    '#form' => \Drupal::service('renderer')->render($form),
  );
  return $render_array;
}

/**
 * hook_page_attachments_alter()
 */
function draggable_blocks_page_attachments_alter(array &$page) {
  if (!_draggable_blocks_validate()) {
    return;
  }
  $theme = \Drupal::theme()->getActiveTheme();
  $regions = NULL;
  $drag_regions = Draggable::draggablePluginManager()->getRegions();
  if (isset($drag_regions[$theme->getName()])) {
    $regions = $drag_regions[$theme->getName()];
  }
  else {
    $base_themes = $theme->getBaseThemes();
    foreach($base_themes as $name => $base_theme) {
      if (isset($drag_regions[$name])) {
        $regions = $drag_regions[$name];
        break;
      }
	}
  }
  $block_list = _draggable_blocks_list_blocks();
  // Delete unnecesary associative array keys.
  if (isset($drag_regions[$theme->getName()]['provider'])) {
    unset($drag_regions[$theme->getName()]['provider']);
  }
  if (isset($drag_regions[$theme->getName()]['id'])) {
    unset($drag_regions[$theme->getName()]['id']);
  }
  $page['#attached']['drupalSettings']['draggable_blocks']['region_list'] = $regions;
  $page['#attached']['drupalSettings']['draggable_blocks']['block_list'] = $block_list[$theme->getName()];
}

/**
 * template_preprocess_theme()
 */
function template_preprocess_draggable_blocks(array &$variables) {
    // ToDo reemplazar la función de arriba
//  $variables['#attached']['library'][] = 'core/modernizr';
}

function _draggable_blocks_validate() {
  if (!\Drupal::currentUser()->hasPermission('access draggable blocks')) {
    return FALSE;
  }
  return TRUE;
}

function _draggable_blocks_render_block($block) {
  $block_id = \Drupal\block\Entity\Block::load($block);
  $block_content = \Drupal::entityManager()
    ->getViewBuilder('block')
    ->view($block_id);
 
  return array('#markup' => drupal_render($block_content));
}

function _draggable_blocks_list_blocks() {
  $storage = \Drupal::entityManager()->getStorage('block');
  $entities = $storage->loadMultiple($ids);
  $blocks = [];
  foreach ($entities as $entity_id => $entity) {
    $definition = $entity->getPlugin()->getPluginDefinition();
    $blocks[$entity->getTheme()][$entity_id] = array(
      'label' => $entity->label(),
      'entity_id' => $entity_id,
      'weight' => $entity->getWeight(),
      'region' => $entity->getRegion(),
      'category' => $definition['category'],
      'definition_id' => $definition['id'],
    );
  }
  return $blocks;
}

function _draggable_blocks_block_definitions() {
  $blockManager = \Drupal::service('plugin.manager.block');
  $contextRepository = \Drupal::service('context.repository');
  // Get blocks definition
  $definitions = $blockManager->getDefinitionsForContexts($contextRepository->getAvailableContexts());
  $blocks = [];
  foreach ($definitions as $definition_id => $definition) {
    $blocks[$definition_id] = array(
      // ToDo Cannot render directly here
      'label' => $definition['admin_label'],
      'id' => '_' . $definition_id,
      'provider' => $definition['provider'],
      'category' => $definition['category'],
    );
  }
  return $blocks;
}

function _draggable_blocks_save_blocks($regions) {
  $theme = \Drupal::theme()->getActiveTheme();
  $storage = \Drupal::entityManager()->getStorage('block');
  $entities = $storage->loadMultiple($ids);
  $blockManager = \Drupal::service('plugin.manager.block');
  $contextRepository = \Drupal::service('context.repository');
  // Get blocks definition
  $definitions = $blockManager->getDefinitionsForContexts($contextRepository->getAvailableContexts());
  foreach($regions as $region => $blocks) {
    // Get initial blocks per region
    // Inject original blocks to region block array (ordering logic)
    // Detect deletions
    foreach($blocks as $weight => $block) {
      if ($block[0] == '_') {
        $definition_id = substr($block, 1);
        _draggable_blocks_enable_block ($definitions[$definition_id]['id'], $weight, $region, $theme->getName());
	  }
      else {
        _draggable_blocks_update_block ($entities[$block], $weight, $region);
	  } 
    }
  }
}

function _draggable_blocks_update_block ($block, $weight, $region) {
  $block->setWeight($weight);
  $block->setRegion($region);
  $block->save();
}

function _draggable_blocks_enable_block ($definition, $weight, $region, $theme) {
  $blockEntityManager = \Drupal::service('entity.manager')->getStorage('block');
  $block = $blockEntityManager->create(
    array(
      'id'=> $definition,
      'plugin' => $definition,
      'theme' => $theme,
      'weight' => $weight
    )
  );
  $block->setRegion($region);
  $block->save();
}

function _draggable_blocks_disable_block ($block) {
  $block->disable();
}